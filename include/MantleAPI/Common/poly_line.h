/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  poly_line.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_POLY_LINE_H
#define MANTLEAPI_COMMON_POLY_LINE_H

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/time_utils.h>

#include <optional>
#include <vector>

namespace mantle_api
{
struct PolyLinePoint
{
  Pose pose{};
  std::optional<Time> time{};

  bool operator==(const PolyLinePoint& other) const
  {
    return other.time == time && other.pose == pose;
  }

  friend std::ostream& operator<<(std::ostream& os, const PolyLinePoint& polyLinePoint);
};

inline std::ostream& operator<<(std::ostream& os, const PolyLinePoint& polyLinePoint)
{
  os << polyLinePoint.pose;

  if (polyLinePoint.time.has_value())
  {
    os << ", time in ms " << polyLinePoint.time.value();
  }

  return os;
}

using PolyLine = std::vector<PolyLinePoint>;

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_POLY_LINE_H