/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  map_details.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_MAP_DETAILS_H
#define MANTLEAPI_MAP_MAP_DETAILS_H

#include <MantleAPI/Common/position.h>

#include <vector>

namespace mantle_api
{
struct MapDetails
{
  virtual ~MapDetails() = default;

  std::vector<Position> map_region;
};

}  // namespace mantle_api
#endif  // MANTLEAPI_MAP_MAP_DETAILS_H
