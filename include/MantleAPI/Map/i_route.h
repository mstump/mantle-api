/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_route.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_I_ROUTE_H
#define MANTLEAPI_MAP_I_ROUTE_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/lane_definition.h>
#include <units.h>

namespace mantle_api
{

class IRoute : public virtual IIdentifiable
{
public:
  virtual IRoute& AddWaypoint(const Vec3<units::length::meter_t>& inert_pos) = 0;
  virtual IRoute& AddWaypoint(Vec3<units::length::meter_t>&& inert_pos) = 0;
  [[nodiscard]] virtual Vec3<units::length::meter_t> GetInertPos(units::length::meter_t route_pos,
                                                                 LaneId lane_id,
                                                                 units::length::meter_t lane_offset = units::length::meter_t{
                                                                     0.0}) const = 0;
  [[nodiscard]] virtual units::length::meter_t GetLaneWidth(units::length::meter_t route_pos, LaneId lane_id) const = 0;
  [[nodiscard]] virtual LaneId GetLaneId(const Vec3<units::length::meter_t>& inert_pos) const = 0;
  [[nodiscard]] virtual units::length::meter_t GetDistanceFromStartTo(const Vec3<units::length::meter_t>& inert_pos) const = 0;
  [[nodiscard]] virtual units::length::meter_t GetLength() const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_MAP_I_ROUTE_H
