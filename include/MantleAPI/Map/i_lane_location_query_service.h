/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_lane_location_query_service.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_I_LANE_LOCATION_QUERY_SERVICE_H
#define MANTLEAPI_MAP_I_LANE_LOCATION_QUERY_SERVICE_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/lane_definition.h>
#include <units.h>

#include <optional>
#include <vector>

namespace mantle_api
{
enum class Direction
{
  kForward = 0,
  kBackwards = 1
};

enum class LateralDisplacementDirection
{
  kAny = 0,
  kLeft = 1,
  kRight = 2
};

/// Abstraction layer for all map related functions.
class ILaneLocationQueryService
{
public:
  virtual ~ILaneLocationQueryService() = default;

  /// TODO: Currently LaneLocationProvider just inherits from this for the sake of easily passing it to the
  /// controllers
  /// Therefore the GetMapObjectById function has been removed from the interface - for now.
  /// We need to think about proper interface functions we want to define here (GetLaneLocation? GetLanes? But this
  /// would add a rat-tail of more interfaces we need to define (ILaneLocation, ILane, ...?)
  // virtual const IIdentifiable& GetMapObjectById(UniqueId id) = 0;

  /// @brief Tries to map a given position to a lane and then returns the orientation of the lane center line at the
  /// given position
  ///
  /// @param position  Position that shall be mapped to a lane
  /// @return Orientation of the lane at this position
  /// @throw If the position cannot be mapped to a lane
  [[nodiscard]] virtual Orientation3<units::angle::radian_t> GetLaneOrientation(
      const Vec3<units::length::meter_t>& position) const = 0;

  /// @brief Shifts a position the given amount upwards along the lane normal. This function is used for shifting
  /// entities upwards, such that their bounding boxes are touching with the bottom-plane the road surface. Thus,
  /// entities do not "sink" into the road.
  ///
  /// @param position  Position on road surface
  /// @param upwards_shift  Amount to shift position upwards along lane normal in [m]
  /// @param allow_invalid_positions  If true and the position cannot be mapped to a lane, then an upwards vector in
  ///                                 world z-direction is used instead of the lane normal for the shift
  /// @return Upwards shifted position
  /// @throw If the position cannot be mapped to a lane and allow_invalid_positions=false.
  [[nodiscard]] virtual Vec3<units::length::meter_t> GetUpwardsShiftedLanePosition(const Vec3<units::length::meter_t>& position,
                                                                                   double upwards_shift,
                                                                                   bool allow_invalid_positions = false) const = 0;

  /// @brief Checks, if a given position can be mapped to a lane
  ///
  /// @param position  Position to be checked
  /// @return true if the position can be mapped to a lane, false otherwise
  [[nodiscard]] virtual bool IsPositionOnLane(const Vec3<units::length::meter_t>& position) const = 0;

  /// @brief Returns a list of IDs representing all lanes enclosing the passed in position within their shape(s).
  ///
  /// @param position  Position to search for the Lane IDs
  /// @return List of global lane IDs
  [[nodiscard]] virtual std::vector<UniqueId> GetLaneIdsAtPosition(const Vec3<units::length::meter_t>& position) const = 0;

  /// @brief Calculate the new pose which is at a certain longitudinal distance from the reference_pose_on_lane
  ///        following a direction.
  ///
  /// @param reference_pose_on_lane  Starting position. Must be on a lane.
  /// @param distance  Distance to go along the lane from the reference_pose_on_lane.
  /// @param direction  Direction to go along the lane from the reference_pose_on_lane. The orientation of the reference
  ///                   pose is used to determine the forward direction. "Forward" means along the (longitudinal) lane
  ///                   direction that is closest to the orientation of the reference pose. The reference pose should
  ///                   not be perpendicular to the lane.
  /// @return Pose that is at the given distance from reference_pose_on_lane in the specified direction. The new
  ///         orientation is parallel to the lane orientation at the target position. The lateral offset from the lane
  ///         center line stays the same as at reference_pose_on_lane. If the reference_pose_on_lane cannot be mapped to
  ///         a lane or the target position would be beyond the road network limits, no value is returned.
  [[nodiscard]] virtual std::optional<Pose> FindLanePoseAtDistanceFrom(const Pose& reference_pose_on_lane,
                                                                       units::length::meter_t distance,
                                                                       Direction direction) const = 0;

  /// @brief Calculate the new pose which is at a certain lateral distance from the reference pose.
  ///
  /// @param reference_pose  Reference pose. Must be on a lane.
  /// @param direction  Lateral displacement direction either to the left or right w.r.t the reference pose.
  ///                   If displacement direction is set to "any", position calculation to the left is executed first and,
  ///                   in case of an an invalid result, to the right next. If both are invalid, no result
  ///                   is returned (std::nullopt).
  /// @param distance  Lateral distance either to left or right of the reference pose.
  /// @return Position at the given lateral distance in the given displacement direction from the reference pose.
  ///         If the calculated position is not on a lane, no value is returned. If the position is on multiple
  ///         lanes at the same time, the first lane in the list is considered.
  [[nodiscard]] virtual std::optional<Vec3<units::length::meter_t>> GetPosition(const mantle_api::Pose& reference_pose,
                                                                                mantle_api::LateralDisplacementDirection direction,
                                                                                units::length::meter_t distance) const = 0;

  /// @brief Calculate the longitudinal distance of two given positions on a lane.
  ///
  /// @param start_position  Starting position. Must be on a lane.
  /// @param target_position Target position. Must be projectable to the lane center line of the start position or one of its' successors.
  /// @return Longitudinal distance of the two positions along the lane center line. If there is more than one possible
  ///         route through the road network to get from start to target the route strategy is currently up to the query
  ///         service implementation.
  ///         No value returned if the distance is not calculable.

  [[nodiscard]] virtual std::optional<units::length::meter_t> GetLongitudinalLaneDistanceBetweenPositions(
      const mantle_api::Vec3<units::length::meter_t>& start_position,
      const mantle_api::Vec3<units::length::meter_t>& target_position) const = 0;

  /// @brief Calculate a new pose which is at a certain longitudinal distance in a relative lane from the
  ///        reference_pose_on_lane.
  ///
  /// @param reference_pose_on_lane  Starting position. Must be on a lane.
  /// @param relative_target_lane  Shift of the target position in number of lanes relative to the lane where the
  ///                              reference pose is located. Positive to the left, negative to the right.
  /// @param distance  The longitudinal distance along the center line of the lane closest to the reference pose. The
  ///                  orientation of the reference pose is used to determine the direction. A positive distance means
  ///                  along the (longitudinal) lane direction that is closest to the orientation of the reference pose.
  ///                  The reference pose should not be perpendicular to the lane.
  /// @param lateral_offset  Lateral offset to the target lane of the new pose
  /// @return Pose that is at the given distance from reference_pose_on_lane in a relative lane. The new orientation is
  ///         parallel to the lane orientation at the target position. No value is returned, if the
  ///         reference_pose_on_lane cannot be mapped to a lane or if the target position would be beyond the road
  ///         network limits in longitudinal or lateral direction.
  [[nodiscard]] virtual std::optional<Pose> FindRelativeLanePoseAtDistanceFrom(const Pose& reference_pose_on_lane,
                                                                               int relative_target_lane,
                                                                               units::length::meter_t distance,
                                                                               units::length::meter_t lateral_offset) const = 0;

  /// @brief Calculate the lane id of the relative target lane from a given position
  ///
  /// @param reference_pose_on_lane  Starting position. Must be on a lane.
  /// @param relative_target_lane  Shift of the target position in number of lanes relative to the lane where the
  ///                              reference pose is located. Positive to the left, negative to the right.
  /// @return Lane id that is at the given lateral shift (relative_lane_target) from given position
  ///         (reference_pose_on_lane). No value, if reference pose is not on a lane or if the lane doesn't have a
  ///         suitable adjacent lane.
  [[nodiscard]] virtual std::optional<mantle_api::LaneId> GetRelativeLaneId(const mantle_api::Pose& reference_pose_on_lane,
                                                                            int relative_lane_target) const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_MAP_I_LANE_LOCATION_QUERY_SERVICE_H
