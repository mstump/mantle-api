#ifndef MANTLEAPI_TRAFFIC_I_CONTROLLER_H
#define MANTLEAPI_TRAFFIC_I_CONTROLLER_H

/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_controller.h */
//-----------------------------------------------------------------------------

#include "MantleAPI/Common/i_identifiable.h"

#pragma once

namespace mantle_api
{
/// Base interface for all controllers.
class IController : public IIdentifiable
{
public:
  /// Activates the controller for the lateral domain
  virtual void ActivateLateral() = 0;

  /// Deactivates the controller for the lateral domain
  virtual void DeactivateLateral() = 0;

  /// Activates the controller for the longitudinal domain
  virtual void ActivateLongitudinal() = 0;

  /// Deactivates the controller for the longitudinal domain
  virtual void DeactivateLongitudinal() = 0;
};

}  // namespace mantle_api
#endif
